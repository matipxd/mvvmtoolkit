﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceArena_Pivot
{
    public class MainPageViewModel : ViewModelBase
    {
        FlickrImagesViewModel firstPage;
        FlickrImagesViewModel secondPage;
        FlickrImagesViewModel thirdPage;
        FlickrImagesViewModel fourthPage;
        FlickrImagesViewModel fifthPage;

        public FlickrImagesViewModel FirstPage
        {
            get
            {
                if (firstPage == null)
                    firstPage = new FlickrImagesViewModel();

                return firstPage;
            }
        }

        public FlickrImagesViewModel SecondPage
        {
            get
            {
                if (secondPage == null)
                    secondPage = new FlickrImagesViewModel();

                return secondPage;
            }
        }
        public FlickrImagesViewModel ThirdPage
        {
            get
            {
                if (thirdPage == null)
                    thirdPage = new FlickrImagesViewModel();

                return thirdPage;
            }
        }
        public FlickrImagesViewModel FourthPage
        {
            get
            {
                if (fourthPage == null)
                    fourthPage = new FlickrImagesViewModel();

                return fourthPage;
            }
        }
        public FlickrImagesViewModel FifthPage
        {
            get
            {
                if (fifthPage == null)
                    fifthPage = new FlickrImagesViewModel();

                return fifthPage;
            }
        }

        public void LoadAllImages()
        {
            FirstPage.GetImages(1);
            SecondPage.GetImages(2);
            ThirdPage.GetImages(3);
            FourthPage.GetImages(4);
            FifthPage.GetImages(5);
        }
    }
}
