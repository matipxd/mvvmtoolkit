﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PerformanceArena_Pivot.FrameSelector
{
    public class MainPageFrameVM : ViewModelBase
    {
        public MainPageFrameVM()
        {
            CurrentPage = new FramePageContent<TestUserControl, FlickrImagesViewModel>(() => new TestUserControl(), () => new FlickrImagesViewModel());

            CurrentPage.Load();
        }

        private FrameContent currentPage;

        public FrameContent CurrentPage
        {
            get 
            {
                return currentPage;
            }

            set
            {
                currentPage = value;

                RaisePropertyChanged("CurrentPage");
            }
        }


        private RelayCommand _ShowCommand;

        public ICommand ShowCommand
        {
            get
            {
                if (_ShowCommand == null)
                {
                    _ShowCommand = new RelayCommand(Show, () => ShowCommandCanExecute);
                }

                return _ShowCommand;
            }
        }

        bool ShowCommandCanExecute
        {
            get
            {
                return _ShowCommandCanExecute;
            }

            set
            {
                if (_ShowCommandCanExecute == value)
                {
                    return;
                }

                _ShowCommandCanExecute = value;

                RaisePropertyChanged("ShowCommandCanExecute");

                if (_ShowCommand != null)
                    _ShowCommand.RaiseCanExecuteChanged();
            }
        }

        private bool _ShowCommandCanExecute = true;

        private void Show()
        {
            CurrentPage = new FramePageContent<SecondUserControl, FlickrImagesViewModel>(() => new SecondUserControl(), () => new FlickrImagesViewModel());

            CurrentPage.Load();
        }
        
    }
}
