﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceArena_Pivot.FrameSelector
{
    public abstract class FrameContent
    {
        public abstract Type GetPageType();

        public abstract Type GetViewModelType();

        public abstract object GetPageInstance();

        public abstract object GetViewModelInstance();

        public Action Loaded;

        public void Load()
        {
            if (Loaded != null)
                Loaded();
        }


    }
}
