﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PerformanceArena_Pivot.FrameSelector
{
    public class FrameControl : ContentControl
    {
        public FrameContent FrameContent
        {
            get { return (FrameContent)GetValue(FrameContentProperty); }
            set { SetValue(FrameContentProperty, value); }
        }

        public static readonly DependencyProperty FrameContentProperty =
            DependencyProperty.Register("FrameContent", typeof(FrameContent), typeof(FrameControl), new PropertyMetadata(null, FrameContentPropertyChanged));

        private static void FrameContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameControl control = d as FrameControl;

            control.LoadContent(e.NewValue as FrameContent);
        }

        public override void OnApplyTemplate()
        {
            if (FrameContent != null)
            {
                content_LoadFrameContent();
            }

            base.OnApplyTemplate();
        }

        private void LoadContent(FrameContent content)
        {
            content.Loaded += content_LoadFrameContent;

            if (DesignerProperties.IsInDesignTool)
            {
                content_LoadFrameContent();
            }
        }

        void content_LoadFrameContent()
        {
            //FrameContent.waitingForInitilization = false;
            this.Content = FrameContent.GetPageInstance();
            (this.Content as UserControl).DataContext = FrameContent.GetViewModelInstance();
        }
    }
}
