﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceArena_Pivot.FrameSelector
{
    public class FrameManagerContent<VM> : FrameContent where VM : class
    {
        private Func<VM> viewModelInstanceCreator;

        public FrameManagerContent()
        {

        }

        public FrameManagerContent(Func<VM> viewModelInstanceCreator)
        {
            this.viewModelInstanceCreator = viewModelInstanceCreator;
        }

        public override object GetPageInstance()
        {
            return null;
        }

        public override Type GetPageType()
        {
            return null;
        }

        public override object GetViewModelInstance()
        {
            throw new NotImplementedException();
        }

        public override Type GetViewModelType()
        {
            throw new NotImplementedException();
        }
    }
}
