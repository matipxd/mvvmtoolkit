﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PerformanceArena_Pivot.FrameSelector
{
    public class FramePageContent<P, VM> : FrameContent where P : UserControl where VM : class
    {
        private Func<P> pageInstanceCreator;
        private Func<VM> viewModelInstanceCreator;

        public FramePageContent()
        {

        }


        public FramePageContent(Func<P> pageInstanceCreator, Func<VM> viewModelInstanceCreator)
        {
            this.pageInstanceCreator = pageInstanceCreator;
            this.viewModelInstanceCreator = viewModelInstanceCreator;
        }

        public override object GetPageInstance()
        {
            if (pageInstanceCreator != null)
                return pageInstanceCreator();
            else
                return null;
        }

        public override Type GetPageType()
        {
            return typeof(P);
        }

        public override object GetViewModelInstance()
        {
            if (viewModelInstanceCreator != null)
                return viewModelInstanceCreator();
            else
                return null;
        }

        public override Type GetViewModelType()
        {
            return typeof(VM);
        }
    }
}
