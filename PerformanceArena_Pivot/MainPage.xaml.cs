﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PerformanceArena_Pivot.Resources;
using System.Diagnostics;

namespace PerformanceArena_Pivot
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            InitializeComponent();
            sw.Stop();
           // MessageBox.Show("User controls ms: " + sw.ElapsedMilliseconds.ToString());

            // Set the data context of the listbox control to the sample data

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            this.Loaded += MainPage_Loaded;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if ((App.Current as App).backTime != null)
            {
                (App.Current as App).backTime.Stop();

                Dispatcher.BeginInvoke( () =>
                    {
                        MessageBox.Show("Back " + (App.Current as App).backTime.ElapsedMilliseconds);
                    }
                );
            }
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (App.Current as App).backTime = null;
            NavigationService.Navigate(new Uri("/TestPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void Hide_click(object sender, RoutedEventArgs e)
        {
            i2.Visibility = System.Windows.Visibility.Collapsed;
            i3.Visibility = System.Windows.Visibility.Collapsed;
            i4.Visibility = System.Windows.Visibility.Collapsed;
            i5.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}