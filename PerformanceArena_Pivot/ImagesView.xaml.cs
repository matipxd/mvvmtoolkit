﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using FlickrNet;
using System.Collections.ObjectModel;

namespace PerformanceArena_Pivot
{
    public partial class ImagesView : PhoneApplicationPage
    {
        public ImagesView()
        {
            InitializeComponent();

            this.DataContext = new MainPageViewModel();
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                (this.DataContext as MainPageViewModel).LoadAllImages();
            }

            base.OnNavigatedTo(e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/TestPage.xaml", UriKind.RelativeOrAbsolute));
        }


    }
}