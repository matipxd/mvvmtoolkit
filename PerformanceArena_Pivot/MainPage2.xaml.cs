﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;

namespace PerformanceArena_Pivot
{
    public partial class MainPage2 : PhoneApplicationPage
    {
        public MainPage2()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            InitializeComponent();
            sw.Stop();
            //MessageBox.Show("Elapsed ms: " + sw.ElapsedMilliseconds.ToString());

            this.Loaded += MainPage_Loaded;

        }


        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if ((App.Current as App).backTime != null)
            {
                (App.Current as App).backTime.Stop();

                Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Back " + (App.Current as App).backTime.ElapsedMilliseconds);
                }
                );
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (App.Current as App).backTime = null;

            NavigationService.Navigate(new Uri("/TestPage.xaml", UriKind.RelativeOrAbsolute));
        }

        private void Hide_click(object sender, RoutedEventArgs e)
        {
            //i2.Visibility = System.Windows.Visibility.Collapsed;
            //i3.Visibility = System.Windows.Visibility.Collapsed;
            //i4.Visibility = System.Windows.Visibility.Collapsed;
            //i5.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}