﻿using FlickrNet;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceArena_Pivot
{
    public class FlickrImagesViewModel : ViewModelBase
    {
        private ObservableCollection<Photo> _Images = new ObservableCollection<Photo>();

        public ObservableCollection<Photo> Images
        {
            get { return _Images; }
            set
            {
                _Images = value;
                RaisePropertyChanged("Images");
            }
        }
        

        public void GetImages(int page)
        {
            Flickr flickrService = new Flickr("a75443eaa6c232fe48ecd3477014a500", "83515b34115dfee1");
            flickrService.PhotosGetRecentAsync(page, 15, (result) =>
            {
                foreach (var item in result.Result)
                {
                    Images.Add(item);
                }
            });
        }
    }
}
