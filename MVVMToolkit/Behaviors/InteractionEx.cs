﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace MVVMToolkit.Behaviors
{
    public class InteractionEx : DependencyObject
    {



        public IList<IAttachedObject> Behaviors
        {
            get { return (IList<IAttachedObject>)GetValue(BehaviorsProperty); }
            set { SetValue(BehaviorsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Behaviors.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BehaviorsProperty =
            DependencyProperty.RegisterAttached("Behaviors", typeof(BehaviorCollection), typeof(InteractionEx), new PropertyMetadata(null, BehaviorsPropertyChanged));

        //public static BehaviorCollection GetBehaviors(DependencyObject item)
        //{
        //    //var collection = (BehaviorCollection)item.GetValue(BehaviorsProperty);

        //    //if (collection == null)
        //    //{
        //    //    collection = new BehaviorCollection();
        //    //    item.SetValue(BehaviorCollection, collection);
        //    //}
        //    //return collection;
        //}

        private static void BehaviorsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

    }
}
