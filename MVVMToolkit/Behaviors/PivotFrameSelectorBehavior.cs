﻿using Microsoft.Phone.Controls;
using MVVMToolkit.Navigation.Frame.Contents;
using MVVMToolkit.Navigation.Frame.Controls;
using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace MVVMToolkit.Behaviors
{
    public class PivotFrameSelectorBehavior : Behavior<Pivot>
    {
        #region Dependency properties

        /// <summary>
        /// Gets or sets IPivotSelector
        /// </summary>
        /// <returns></returns>
        public OneActivePageManagerViewModel PivotSelector
        {
            get { return (OneActivePageManagerViewModel)GetValue(PivotSelectorProperty); }
            set { SetValue(PivotSelectorProperty, value); }
        }

        private SemaphoreSlim synchronization = new SemaphoreSlim(1);
        private bool isRegistered;

        private Dictionary<FrameManager, PivotItem> cacheAssosiation = new Dictionary<FrameManager, PivotItem>();
        private Dictionary<string, PivotItem> frameManagers = new Dictionary<string, PivotItem>();

        public static readonly DependencyProperty PivotSelectorProperty =
            DependencyProperty.Register("PivotSelector", typeof(OneActivePageManagerViewModel), typeof(PivotFrameSelectorBehavior), new PropertyMetadata(null, PivotSelectorPropertyChanged));

        private static void PivotSelectorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PivotFrameSelectorBehavior control = d as PivotFrameSelectorBehavior;

            if (e.OldValue != null)
                (e.OldValue as OneActivePageManagerViewModel).SelectedFrameManagerChanged -= control.viewModel_SelectedFrameManagerChanged;

            if (e.NewValue != null)
            {
                (e.NewValue as OneActivePageManagerViewModel).SelectedFrameManagerChanged += control.viewModel_SelectedFrameManagerChanged;
                control.viewModel_SelectedFrameManagerChanged((e.NewValue as OneActivePageManagerViewModel).SelectedManager);
            }
        }


        #endregion


        void viewModel_SelectedFrameManagerChanged(Navigation.Frame.Contents.FrameManager obj)
        {
            PivotItem selectedPivotItem = null;

            if (obj == null)
                return;

            if (cacheAssosiation.TryGetValue(obj, out selectedPivotItem))
            {
                if (this.AssociatedObject.SelectedItem != selectedPivotItem)
                    this.AssociatedObject.SelectedItem = selectedPivotItem;
            }
            else
            {
                var foundFrameControl = GetChildrenRecursive(this.AssociatedObject).OfType<FrameControl>().FirstOrDefault(x => x.Manager == obj);

                if (foundFrameControl != null)
                {
                    var foundPivotItem = FindVisualAncestor<PivotItem>(foundFrameControl);

                    if (foundPivotItem != null)
                    {
                        this.AssociatedObject.SelectedItem = foundPivotItem;

                        cacheAssosiation.Add(obj, foundPivotItem);
                    }
                }
            }
        }

        void SelectedFrameManagerChanged(string frameManagerName)
        {
            var foundFrameManager = frameManagers.FirstOrDefault(x => x.Key == frameManagerName);
            this.AssociatedObject.SelectedItem = foundFrameManager.Value;
        }

        private void RegisterFrames()
        {
            foreach (var item in AssociatedObject.Items)
            {
                var foundFrameControl = FindChildByType<FrameControl>((item as PivotItem).Content as DependencyObject);

                if (foundFrameControl != null)
                {
                    frameManagers.Add(foundFrameControl.ManagerName, (item as PivotItem));
                    foundFrameControl.FrameManagerChanged += FoundFrameControl_FrameManagerChanged;
                }
            }
        }

        private void FoundFrameControl_FrameManagerChanged(FrameControl arg1, FrameManager arg2)
        {
            if (PivotSelector.SelectedManager == arg2)
            {
                PivotSelector.ChangeActiveFrameManager(arg2);
                SelectedFrameManagerChanged(arg2.Name);
            }
        }

        protected override void OnAttached()
        {
            base.AssociatedObject.LoadedPivotItem += AssociatedObject_LoadedPivotItem;
            base.OnAttached();
        }

        private FrameControl previousFrameControl;

        private async void AssociatedObject_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            if (!isRegistered)
            {
                isRegistered = true;
                RegisterFrames();
            }

            if (PivotSelector != null)
            {
                try
                {
                    await synchronization.WaitAsync();

                    if (e.Item != null && e.Item.Content != null && e.Item.Content is DependencyObject)
                    {
                        var foundFrameControl = FindChildByType<FrameControl>(e.Item.Content as DependencyObject);

                        if (foundFrameControl != null)
                        {
                            if (foundFrameControl.Manager != null)
                                PivotSelector.ChangeActiveFrameManager(foundFrameControl.Manager);
                            else
                            {
                                if (PivotSelector.SelectedManager != null && PivotSelector.SelectedManager.Name != foundFrameControl.ManagerName)
                                {
                                    SelectedFrameManagerChanged(PivotSelector.SelectedManager.Name);
                                }
                            }
                        }
                    }
                }
                finally
                {
                    synchronization.Release();
                }
            }
        }


        private T FindChildByType<T>(DependencyObject element) where T : DependencyObject
        {
            if (element != null && element.GetType() == typeof(T))
                return element as T;
            else
                return GetChildrenRecursive(element).OfType<T>().FirstOrDefault();
        }


        private IEnumerable<DependencyObject> GetChildrenRecursive(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                var child = VisualTreeHelper.GetChild(element, i);

                yield return child;

                foreach (var item in GetChildrenRecursive(child))
                {
                    yield return item;
                }
            }
        }

        public T FindVisualAncestor<T>(DependencyObject child, Predicate<DependencyObject> condition = null) where T : class
        {
            if (child == null)
            {
                return null;
            }

            if (condition == null)
            {
                condition = (item) => { return item is T; };
            }

            DependencyObject parent = VisualTreeHelper.GetParent(child);
            while (parent != null)
            {
                if (condition(parent))
                {
                    return parent as T;
                }

                parent = VisualTreeHelper.GetParent(parent);
            }

            return null;
        }
    }
}
