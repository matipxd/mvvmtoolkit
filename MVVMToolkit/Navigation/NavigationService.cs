﻿using Microsoft.Phone.Controls;
using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace MVVMToolkit.Navigation
{
    public class NavigationService : INavigationService
    {
        private Stack<NavigationParameter> parametersStack = new Stack<NavigationParameter>();
        private object returningValue = null;
        private PhoneApplicationFrame phoneFrame = null;
        private bool isReturningResult = false;
        private Dispatcher dispatcher = null;
        private bool navigationFromTombstone = false;
        private object lockSync = new object();

        public event Action FrameNavigated;

        public IEnumerable<JournalEntry> navigationBackStack
        {
            get
            {
                return phoneFrame.BackStack;
            }
        }

        public bool IsNavigationFormTombstone
        {
            get
            {

                return navigationFromTombstone;
            }
        }

        public NavigationService(PhoneApplicationFrame phoneFrame, Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            this.phoneFrame = phoneFrame;

            this.phoneFrame.Navigated += PhoneFrame_Navigated;
        }

        private void PhoneFrame_Navigated(object sender, NavigationEventArgs e)
        {
            if (FrameNavigated != null)
                FrameNavigated();
        }

        public void CreateAndOpenDataStream()
        {
            //throw new NotImplementedException();
        }

        public bool TryRestoreFromTombstone()
        {
            throw new NotImplementedException();
        }

        public object RestoreLastParamater()
        {
            throw new NotImplementedException();
        }


        public void SaveIncrementalyData()
        {
            //throw new NotImplementedException();
        }

        public void RemoveLastParameter()
        {
            //throw new NotImplementedException();
        }

        public object GetNavigationParameter()
        {
            lock (lockSync)
            {
                navigationFromTombstone = false;

                if (parametersStack.Count > 0)
                {
                    var lastParamater = parametersStack.Peek();

                    if (!lastParamater.IsLoaded)
                    {
                        throw new NotImplementedException();
                    }

                    return lastParamater.Data;
                }
                else
                    return null;
            }
        }

        public object GetReturningValue()
        {
            lock (lockSync)
            {
                if (!isReturningResult)
                    return null;

                isReturningResult = false;

                return returningValue;
            }
        }


        public void Navigate(Uri uriToNavigate, object parameters, bool removeFromNavigation = false)
        {
            try
            {
                dispatcher.BeginInvoke(() =>
                {
                    try
                    {
                        lock (lockSync)
                        {
                            isReturningResult = false;

                            parametersStack.Push(new NavigationParameter()
                            {
                                IsLoaded = true,
                                Data = parameters
                            });
                        }
                        phoneFrame.Navigate(uriToNavigate);

                        if (phoneFrame.BackStack.Count() > 0 && removeFromNavigation)
                        {
                            phoneFrame.RemoveBackEntry();
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        if (parametersStack.Count > 0)
                            parametersStack.Pop();
                    }
                });
            }
            catch (InvalidOperationException)
            {
                if (parametersStack.Count > 0)
                    parametersStack.Pop();
            }
        }

        public void Navigate(Uri uriToNavigate)
        {
            Navigate(uriToNavigate, null);
        }

        public void Navigate<VM>() where VM : IPageViewModel
        {
            Navigate<VM>(null);
        }

        public void Navigate<VM>(object parameters, bool removeFromNavigation = false) where VM : IPageViewModel
        {
            Navigate(PageManager.Instance.GetPhoneApplicationPagePath(typeof(VM)), parameters, removeFromNavigation);
        }

        public void RemoveBackEntry()
        {
            if (phoneFrame.CanGoBack)
                phoneFrame.RemoveBackEntry();
        }

        public void GoBack()
        {
            try
            {
                if (phoneFrame.CanGoBack)
                    phoneFrame.GoBack();
            }
            catch (InvalidOperationException)
            {
            }
        }

        public void GoBack(object returningValue)
        {
            lock (lockSync)
            {
                isReturningResult = true;
                this.returningValue = returningValue;
            }

            try
            {
                if (phoneFrame.CanGoBack)
                    phoneFrame.GoBack();
            }
            catch (InvalidOperationException)
            {
            }
        }

        public bool CanGoBack()
        {
            return phoneFrame.CanGoBack && phoneFrame.BackStack != null && phoneFrame.BackStack.Count() > 0;
        }

        public void AddReturningValue(object parameter)
        {
            lock (lockSync)
            {
                isReturningResult = true;
                this.returningValue = parameter;
            }
        }

        public void Navigate(Type pageViewModelType)
        {
            Navigate(PageManager.Instance.GetPhoneApplicationPagePath(pageViewModelType), null);
        }

        public void Navigate<VM>(Dictionary<string, string> queryStrings) where VM : IPageViewModel
        {
            Navigate(PageManager.Instance.GetPhoneApplicationPagePath(typeof(VM)), queryStrings);
        }

        public void Navigate(Uri uriToNavigate, object parameters, Dictionary<string, string> queryStrings)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("?");

            foreach (var item in queryStrings)
            {
                sb.AppendFormat("{0}={1}&", item.Key, item.Value);
            }

            Navigate(new Uri(uriToNavigate.ToString() + sb.ToString(), UriKind.RelativeOrAbsolute), parameters);
        }

        public void Navigate<VM>(object parameters, Dictionary<string, string> queryStrings) where VM : IPageViewModel
        {
            Navigate(PageManager.Instance.GetPhoneApplicationPagePath(typeof(VM)), parameters, queryStrings);
        }

        public void GoHome()
        {
            try
            {
                while (phoneFrame.BackStack.Count() > 1)
                {
                    if (phoneFrame.CanGoBack)
                        phoneFrame.RemoveBackEntry();
                }

                if (phoneFrame.CanGoBack)
                    phoneFrame.GoBack();

                parametersStack.Clear();
            }
            catch (Exception)
            {
            }
        }

        public void ClearStack()
        {
            try
            {
                while (phoneFrame.BackStack.Count() > 1)
                {
                    if (phoneFrame.CanGoBack)
                        phoneFrame.RemoveBackEntry();
                }

                parametersStack.Clear();
            }
            catch (Exception)
            {
            }
        }
    }
}
