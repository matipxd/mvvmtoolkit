﻿using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace MVVMToolkit.Navigation
{
    public interface INavigationService
    {
        object GetNavigationParameter();

        object GetReturningValue();

        void RemoveLastParameter();

        bool TryRestoreFromTombstone();

        void CreateAndOpenDataStream();

        bool IsNavigationFormTombstone
        {
            get;
        }

        IEnumerable<JournalEntry> navigationBackStack
        {
            get;
        }

        void AddReturningValue(object parameter);

        void Navigate(Uri uriToNavigate, object parameters, bool removeFromNavigation = false);

        void Navigate(Uri uriToNavigate);

        void Navigate(Uri uriToNavigate, object parameters, Dictionary<string, string> queryStrings);

        void Navigate(Type pageViewModelType);

        void Navigate<VM>() where VM : IPageViewModel;

        void Navigate<VM>(object parameters, bool removeFromNavigation = false) where VM : IPageViewModel;

        void Navigate<VM>(object parameters, Dictionary<string, string> queryStrings) where VM : IPageViewModel;

        void GoBack();

        void GoHome();

        void RemoveBackEntry();

        void GoBack(object returningValue);

        bool CanGoBack();

        void ClearStack();
    }
}
