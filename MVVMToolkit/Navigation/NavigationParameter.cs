﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkit.Navigation
{
    internal class NavigationParameter
    {
        public bool IsLoaded { get; set; }

        public object Data { get; set; }
    }
}
