﻿using Microsoft.Phone.Controls;
using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MVVMToolkit.Navigation
{
    internal class PageItem
    {
        public Type viewModelType { get; set; }

        public Type pageType { get; set; }

        public Func<UserControl> pageFactory { get; set; }

        public Uri phoneApplicationPagePath { get; set; }
    }

    public class PageManager
    {
        private static PageManager instance;

        public static PageManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PageManager();
                }
                return instance;
            }
        }

        private List<PageItem> registeredPages = new List<PageItem>();

        public PhoneApplicationFrame TopFrame { get; set; }

        public void Register<U, VM>() where U : UserControl where VM : IPageViewModel
        {
            Register<U, VM>(null);
        }

        public void Unregister<U, VM>()
        {
            var foundRegisteredPage = registeredPages.FirstOrDefault(x => x.pageType == typeof(U) && x.viewModelType == typeof(VM));

            if (foundRegisteredPage != null)
            {
                registeredPages.Remove(foundRegisteredPage);
            }
        }

        public void Register<U, VM>(Func<UserControl> pageFactory) where U : UserControl where VM : IPageViewModel
        {
            if (registeredPages.Any(x => x.pageType == typeof(U) && x.viewModelType == typeof(VM)))
                return;

            registeredPages.Add(new PageItem()
            {
                pageFactory = pageFactory,
                pageType = typeof(U),
                viewModelType = typeof(VM),
                phoneApplicationPagePath = null
            });
        }

        public void Register<VM>(Uri phoneApplicationPagePath) where VM : IPageViewModel
        {
            if (registeredPages.Any(x => x.viewModelType == typeof(VM) && x.phoneApplicationPagePath != null))
                return;

            registeredPages.Add(new PageItem()
            {
                phoneApplicationPagePath = phoneApplicationPagePath,
                viewModelType = typeof(VM)
            });
        }

        public Uri GetPhoneApplicationPagePath(Type viewModel)
        {
            var foundPageItem = registeredPages.Where(x => viewModel.IsAssignableFrom(x.viewModelType) && x.phoneApplicationPagePath != null).FirstOrDefault();

            if (foundPageItem == null)
                throw new ArgumentException(string.Format("ViewModel {0} is not registered", viewModel));

            if (foundPageItem.phoneApplicationPagePath == null)
                throw new ArgumentException(string.Format("Registered {0} doesn't contain URL to specified page", viewModel));

            return foundPageItem.phoneApplicationPagePath;
        }

        public UserControl GetPageInstance(Type viewModel)
        {
            var foundPageItem = registeredPages.Where(x => viewModel.IsAssignableFrom(x.viewModelType)).FirstOrDefault();

            if (foundPageItem == null)
                throw new ArgumentException(string.Format("ViewModel {0} is not registered", viewModel));

            if (foundPageItem.pageFactory != null)
                return foundPageItem.pageFactory();
            else
                throw new ArgumentException(string.Format("Factory for specified page assosiated with {0} doesnt exist", viewModel));
        }
    }
}
