﻿using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MVVMToolkit.Navigation.Frame.Contents
{
    public class FrameContent<VM> : IFrameContent where VM : IPageViewModel
    {
        private Func<IPageViewModel> viewModelFactory;
        private IPageViewModel viewModel;

        public FrameContent(Func<IPageViewModel> viewModelFactory)
        {
            this.viewModelFactory = viewModelFactory;
        }

        public IPageViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                    viewModel = viewModelFactory();

                return viewModel;
            }
        }

        public Type GetViewModelType()
        {
            return typeof(VM);
        }

        public bool IsLoaded
        {
            get;
            set;
        }

        public event Action Loaded;

        public void Load()
        {
            IsLoaded = true;

            if (Loaded != null)
                Loaded();
        }

        public object GetViewModelInstance()
        {
            return viewModelFactory();
        }

    }
}
