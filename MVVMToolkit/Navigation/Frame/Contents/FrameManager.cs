﻿using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace MVVMToolkit.Navigation.Frame.Contents
{
    internal class NavigationModel
    {
        public Type viewModelType { get; set; }

        public object parameter { get; set; }
    }

    public class FrameManager
    {
        public FrameManager()
        {

        }

        public FrameManager(string name, Type startingViewModelType)
        {
            this.startingViewModelType = startingViewModelType;
            Name = name;
        }

        private Stack<NavigationParameter> parametersStack = new Stack<NavigationParameter>();
        private object returningValue = null;
        private object lockSync = new object();
        private bool isReturningResult = false;

        public string Name { get; private set; }

        public Type startingViewModelType
        {
            get;
            internal set;
        }

        public bool IsFirstPageLoaded
        {
            get;
            set;
        }

        public bool IsInitialized
        {
            get;
            set;
        }

        internal event Action<Type, object, bool, bool> Navigated;
        internal Func<bool> _CanGoBack;
        internal Func<IPageViewModel> _currentPageViewModel;
        internal event Action<object> _GoBackWithValue;
        internal event Action _GoBack;
        //internal event Action<Type, object> GoBack;
        internal NavigationModel delayedNavigation = null;

        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                if (_currentPageViewModel != null)
                    return _currentPageViewModel();
                else
                    return null;
            }
        }

        public void Navigate<T>(object parameters, bool resetNavigationStack, bool keepInstanceLive = false) where T : IPageViewModel
        {
            lock (lockSync)
            {
                isReturningResult = false;

                parametersStack.Push(new NavigationParameter()
                {
                    IsLoaded = true,
                    Data = parameters
                });
            }

            if (Navigated != null)
                Navigated(typeof(T), parameters, resetNavigationStack, keepInstanceLive);


        }

        public void AddReturningValue(object parameter)
        {
            lock (lockSync)
            {
                isReturningResult = true;
                this.returningValue = parameter;
            }
        }

        public void Navigate<T>() where T : IPageViewModel
        {
            Navigate<T>(null, false);
        }

        public void NavigateToStartingPageViewModel(object parameter)
        {
            Navigate(startingViewModelType, parameter);
        }

        public void Navigate(Type viewModel)
        {
            if (Navigated != null)
                Navigated(viewModel, null, false, false);
        }

        public void Navigate(Type viewModel, object parameter)
        {
            if (Navigated != null)
                Navigated(viewModel, parameter, false, false);
            else
            {
                delayedNavigation = new NavigationModel()
                {
                    viewModelType = viewModel,
                    parameter = parameter
                };
            }
        }

        public void GoBack(object returningValue)
        {
            if (_GoBackWithValue != null)
                _GoBackWithValue(returningValue);
        }

        public void GoBack()
        {
            if (_GoBack != null)
                _GoBack();
        }


        public bool CanGoBack()
        {
            return _CanGoBack();
        }
        public object GetReturningValue()
        {
            lock (lockSync)
            {
                if (!isReturningResult)
                    return null;

                isReturningResult = false;

                return returningValue;
            }
        }

    }
}
