﻿using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkit.Navigation.Frame.Contents
{
    public interface IFrameContent
    {
        IPageViewModel ViewModel
        {
            get;
        }

        Type GetViewModelType();

        event Action Loaded;

        void Load();

        bool IsLoaded
        {
            get;
            set;
        }
    }
}
