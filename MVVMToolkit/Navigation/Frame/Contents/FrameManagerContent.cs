﻿using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkit.Navigation.Frame.Contents
{
    public class FrameManagerContent<VM> : FrameContent where VM : IPageViewModel
    {
        private Func<VM> viewModelInstanceCreator;

        public FrameManagerContent()
        {

        }

        public FrameManagerContent(Func<VM> viewModelInstanceCreator)
        {
            this.viewModelInstanceCreator = viewModelInstanceCreator;
        }
        public override System.Windows.Controls.UserControl Page
        {
            get { return null; }
        }

        public override IPageViewModel ViewModel
        {
            get
            {
                if (viewModel == null)
                {
                    if (viewModelInstanceCreator != null)
                        viewModel = viewModelInstanceCreator();
                }

                return viewModel;
            }
        }
    }
}
