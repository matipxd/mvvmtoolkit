﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MVVMToolkit.IoC;
using MVVMToolkit.Navigation.Frame.Contents;
using MVVMToolkit.ViewModels;
using MVVMToolkit.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace MVVMToolkit.Navigation.Frame.Controls
{
    internal class FrameContentItem
    {
        public Type viewModelType { get; set; }

        public IPageViewModel viewModel { get; set; }

        public UserControl page { get; set; }

        public bool strongInstance { get; set; }
    }

    public class FrameControl : ContentControl
    {
        private Stack<FrameContentItem> contentCollection = new Stack<FrameContentItem>();
        private bool lazyNavigation = false;
        private PhoneApplicationPage rootApplicationPage = null;
        private bool isLoaded = false;
        private SemaphoreSlim lockSynchrozation = new SemaphoreSlim(1);
        private List<FrameContentItem> strongInstances = new List<FrameContentItem>();

        public static string DebugData = "Not initialized";

        public FrameManager Manager
        {
            get { return (FrameManager)GetValue(ManagerProperty); }
            set { SetValue(ManagerProperty, value); }
        }

        public event Action<FrameControl, FrameManager> FrameManagerChanged;

        public static readonly DependencyProperty ManagerProperty =
            DependencyProperty.Register("Manager", typeof(FrameManager), typeof(FrameControl), new PropertyMetadata(null, FrameManagerPropertyChanged));

        public static readonly DependencyProperty ManagerNameProperty =
            DependencyProperty.Register("ManagerName", typeof(string), typeof(FrameControl), new PropertyMetadata(null));


        public IPageViewModel CurrentPageViewModel
        {
            get { return (IPageViewModel)GetValue(CurrentPageViewModelProperty); }
            set { SetValue(CurrentPageViewModelProperty, value); }
        }

        public string ManagerName
        {
            get { return (string)GetValue(ManagerNameProperty); }
            set { SetValue(ManagerNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentPageViewModel.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentPageViewModelProperty =
            DependencyProperty.Register("CurrentPageViewModel", typeof(IPageViewModel), typeof(FrameControl), new PropertyMetadata(null));

        private static void CurrentPageViewModelPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameControl control = d as FrameControl;

            if (control.isLoaded)
                control.Navigate(e.NewValue.GetType(), false);
        }

        private static void FrameManagerPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameControl control = d as FrameControl;

            if (e.OldValue != null)
                control.Unregister(e.OldValue as FrameManager);

            //control.Register(e.NewValue as FrameManager);

            //if (control.FrameManagerChanged != null)
            //    control.FrameManagerChanged(control, e.NewValue as FrameManager);
        }

        private void Register(FrameManager manager)
        {
            manager.Navigated += Manager_Navigated;
            manager._CanGoBack += Manager__CanGoBack;
            manager._GoBack += GoBack;
            manager._GoBackWithValue += GoBackWithValue;
            manager._currentPageViewModel += GetCurrentPageViewModel; 

            if (DesignerProperties.IsInDesignTool)
            {
                lazyNavigation = false;
                manager.IsFirstPageLoaded = true;
                Navigate(manager.startingViewModelType, false);
                return;
            }
            
            if ((lazyNavigation && manager.startingViewModelType != null) || manager.delayedNavigation != null)
            {
                lazyNavigation = false;
                manager.IsFirstPageLoaded = true;
                manager.Navigate(manager.delayedNavigation.viewModelType, manager.delayedNavigation.parameter);
            }
        }

        private void GoBackWithValue(object returningValue)
        {
            GoBack();
        }


        private bool Manager__CanGoBack()
        {
            //if (contentCollection.Count > 0)
            //    return contentCollection.Peek().viewModel.CanGoBack();

            return CanGoBack();
        }

        private void Unregister(FrameManager manager)
        {
            manager.Navigated -= Manager_Navigated;
            manager._CanGoBack -= Manager__CanGoBack;
            manager._GoBack -= GoBack;
            manager._GoBackWithValue -= GoBackWithValue;
            manager._currentPageViewModel -= GetCurrentPageViewModel; 

        }

        private IPageViewModel GetCurrentPageViewModel()
        {
            return CurrentPageViewModel;
        }

        private async void Manager_Navigated(Type viewModel, object arg2, bool resetNavigationStack, bool keepInstanceLive)
        {
            if (contentCollection.Count > 0)
            {
                contentCollection.First().viewModel.Deactivated();
                contentCollection.First().viewModel.IsActive = false;
                contentCollection.First().viewModel.OnNavigatedFrom(new NavigationEventArgs(null, null, NavigationMode.New, true));
            }

            if (Manager != null)
                Manager.IsFirstPageLoaded = true;

            if (resetNavigationStack)
            {
                contentCollection.Clear();

                if (Content != null)
                {
                    //remove all non string instances
                    var pagesCounts = (Content as Grid).Children.Count;

                    for (int i = 0; i < pagesCounts; i++)
                    {
                        var isStrongInstance = strongInstances.Any(x => x.page == (Content as Grid).Children[i]);

                        if (!isStrongInstance)
                        {
                            (Content as Grid).Children[i].Visibility = Visibility.Collapsed;
                            (Content as Grid).Children.RemoveAt(i);
                            pagesCounts--;
                        }
                        else
                            (Content as Grid).Children[i].Visibility = Visibility.Collapsed;
                    }
                }
            }

            Navigate(viewModel, keepInstanceLive);

            if (!DesignerProperties.IsInDesignTool)
            {
                if (contentCollection.Count > 0)
                {
                    try
                    {
                        contentCollection.Peek().viewModel.IsActive = true;
                        contentCollection.Peek().viewModel.OnNavigatedTo(new NavigationEventArgs(null, null, NavigationMode.New, true), arg2);
                    }
                    finally
                    {
                        try
                        {
                            if (!contentCollection.Peek().viewModel.IsLoaded)
                            {
                                contentCollection.Peek().viewModel.IsLoaded = true;
                                contentCollection.Peek().viewModel.Loaded(arg2);
                            }
                        }
                        finally
                        {
                            try
                            {

                                lockSynchrozation.Wait();
                                contentCollection.Peek().viewModel.Activated(true);
                            }
                            finally
                            {
                                lockSynchrozation.Release();
                            }
                        }
                    }
                }
            }
        }

        private static void FrameContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameControl control = d as FrameControl;

            control.LoadContent(e.NewValue as IFrameContent);
        }

        //public IPageViewModel CurrentPageViewModel
        //{
        //    get
        //    {
        //        if (contentCollection.Count > 0)
        //            return contentCollection.Peek().viewModel;
        //        else
        //            return null;
        //    }
        //}

        private void InitializeStaticContent(Type viewModelType)
        {
            UserControl pageInstance = Content as UserControl;

            FrameContentItem newItem = new FrameContentItem()
            {
                page = pageInstance,
                viewModelType = viewModelType
            };

            contentCollection.Push(newItem);

            var viewModel = pageInstance.DataContext as IPageViewModel; //SimpleIoc.Default.GetOrChangeInstance(viewModelType) as IPageViewModel;

            newItem.viewModel = viewModel;
            //newItem.page.DataContext = viewModel;

            viewModel.View = newItem.page;

            if (rootApplicationPage == null)
            {
                rootApplicationPage = FindTopApplicationPage();

                if (rootApplicationPage == null)
                {
                    throw new ArgumentNullException("RootApplicationPage is null");
                }
            }

            viewModel.RootView = rootApplicationPage;

            if (rootApplicationPage != null)
            {
                if (rootApplicationPage.ApplicationBar != null)
                {
                    viewModel.ApplicationBar = rootApplicationPage.ApplicationBar;
                    DebugData = "Initialized";
                }
                else
                {
                    DebugData = string.Format("RootPage: {0} FramePage: {1}", rootApplicationPage.ToString(), viewModel.View.ToString());
                }

                viewModel.NavigationContext = rootApplicationPage.NavigationContext;
            }

            viewModel.RootFrame = Manager;

            CurrentPageViewModel = viewModel;

            Manager.IsInitialized = true;
        }

        public void Navigate(Type viewModelType, bool keepInstanceLive)
        {
            if (Content != null && Content is UserControl)
            {
                InitializeStaticContent(viewModelType);
                return;
            }

            UserControl pageInstance = null;
            bool createdStrongPage = false;

            if (keepInstanceLive)
            {
                var foundStrongInstance = strongInstances.FirstOrDefault(x => x.viewModelType == viewModelType);

                if (foundStrongInstance != null)
                {
                    pageInstance = foundStrongInstance.page;
                }
                else
                {
                    pageInstance = PageManager.Instance.GetPageInstance(viewModelType);

                    strongInstances.Add(new FrameContentItem()
                    {
                        page = pageInstance,
                        viewModelType = viewModelType
                    });

                    createdStrongPage = true;
                }
            }

            if (pageInstance == null)
                pageInstance = PageManager.Instance.GetPageInstance(viewModelType);

            FrameContentItem newItem = new FrameContentItem()
            {
                page = pageInstance,
                viewModelType = viewModelType,
                strongInstance = keepInstanceLive
            };


            if (contentCollection.Count > 0)
                contentCollection.Peek().page.Visibility = Visibility.Collapsed;

            contentCollection.Push(newItem);

            Canvas.SetZIndex(newItem.page, contentCollection.Count + 1);

            if (this.Content == null)
                this.Content = new Grid();

            var viewModel = SimpleIoc.Default.GetOrChangeInstance(viewModelType) as IPageViewModel;

            newItem.viewModel = viewModel;

            newItem.page.DataContext = viewModel;

            viewModel.View = newItem.page;

            if (!keepInstanceLive || createdStrongPage)
                (this.Content as Grid).Children.Add(newItem.page);

            Canvas.SetZIndex(newItem.page, 0);

            foreach (var item in (this.Content as Grid).Children)
            {
                item.Visibility = Visibility.Collapsed;
            }

            newItem.page.Visibility = Visibility.Visible;

            if (rootApplicationPage == null)
            {
                rootApplicationPage = FindTopApplicationPage();

                if (rootApplicationPage == null)
                {
                    throw new ArgumentNullException("RootApplicationPage is null");
                }
            }

            if (rootApplicationPage != null)
            {
                if (rootApplicationPage.ApplicationBar != null)
                {
                    viewModel.ApplicationBar = rootApplicationPage.ApplicationBar;
                    DebugData = "Initialized";
                }
                else
                {
                    DebugData = string.Format("RootPage: {0} FramePage: {1}", rootApplicationPage.ToString(), viewModel.View.ToString());
                }

                viewModel.NavigationContext = rootApplicationPage.NavigationContext;
            }
            viewModel.RootView = rootApplicationPage;

            viewModel.RootFrame = Manager;

            CurrentPageViewModel = viewModel;

            Manager.IsInitialized = true;
        }

        private PhoneApplicationPage FindTopApplicationPage()
        {
            var parent = VisualTreeHelper.GetParent(this);

            while (!(parent is PhoneApplicationPage))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            return parent as PhoneApplicationPage;
        }

        public bool CanGoBack()
        {
            if (contentCollection.Count > 1)
                return true;
            else
                return false;
        }

        public void GoBack()
        {
            if (contentCollection.Count > 0)
            {
                if (contentCollection.Count > 0)
                {
                    if (contentCollection.First().viewModel == null)
                        throw new ArgumentNullException("contentCollection.First() is null");

                    contentCollection.First().viewModel.IsActive = false;
                    contentCollection.First().viewModel.Deactivated();

                    contentCollection.First().viewModel.OnNavigatedFrom(new NavigationEventArgs(null, null, NavigationMode.Back, true));
                }

                var lastItem = contentCollection.Pop();

                if (lastItem.viewModel == null)
                    throw new ArgumentNullException("LastItem.ViewModel is null");

                if (contentCollection.Count > 0)
                {
                    if (Manager == null)
                        throw new ArgumentNullException("Manager is null");

                    contentCollection.First().viewModel.OnNavigatedTo(new NavigationEventArgs(null, null, NavigationMode.Back, true), Manager.GetReturningValue());

                    contentCollection.First().viewModel.IsActive = true;
                    contentCollection.First().viewModel.Activated(false);

                    CurrentPageViewModel = contentCollection.First().viewModel;
                }
                else
                    CurrentPageViewModel = null;

                if (lastItem.page.DataContext != null)
                    SimpleIoc.Default.Unregister(lastItem.page.DataContext.GetType());

                lastItem.page.DataContext = null;

                if (!lastItem.strongInstance)
                {
                    if (Content == null)
                        throw new ArgumentNullException("Content is null");

                    (Content as Grid).Children.Remove(lastItem.page);
                }

                if (lastItem.page != null)
                    lastItem.page.Visibility = Visibility.Collapsed;

                if (contentCollection.Peek().page != null)
                    contentCollection.Peek().page.Visibility = Visibility.Visible;

                //GC.Collect();
            }
            else
            {
                if (rootApplicationPage != null)
                {
                    rootApplicationPage.NavigationService.GoBack();
                }
            }
        }

        public override void OnApplyTemplate()
        {
            this.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
            this.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;

            if (this.Content == null)
                this.Content = new Grid();

            isLoaded = true;

            if (Manager != null)
            {
                Register(Manager);

                if (FrameManagerChanged != null)
                    FrameManagerChanged(this, Manager);
            }

            base.OnApplyTemplate();
        }

        public void NavigateToStartingPage()
        {
            if (Manager != null && Manager.startingViewModelType != null)
            {
                Navigate(Manager.startingViewModelType, false);
            }
            else
                lazyNavigation = true;
        }

        private void LoadContent(IFrameContent content)
        {
            //content.Loaded += content_LoadFrameContent;
               
            //if (DesignerProperties.IsInDesignTool)
            //{
            //    content_LoadFrameContent();
            //}
        }

        //async void content_LoadFrameContent()
        //{
        //    FrameContent.IsLoaded = true;

        //    this.Content = PageManager.Instance.GetPageInstance(FrameContent.GetViewModelType());
        //    (this.Content as UserControl).DataContext = FrameContent.ViewModel;

        //    await FrameContent.ViewModel.Loaded(null);
        //}
    }
}
