﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using MVVMToolkit.ViewModels;

namespace MVVMToolkit.Navigation
{
    public class NavigationServiceDesign : INavigationService
    {
        public NavigationServiceDesign()
        {

        }

        public bool IsNavigationFormTombstone
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<JournalEntry> navigationBackStack
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void AddReturningValue(object parameter)
        {
            throw new NotImplementedException();
        }

        public bool CanGoBack()
        {
            throw new NotImplementedException();
        }

        public void ClearStack()
        {
            throw new NotImplementedException();
        }

        public void CreateAndOpenDataStream()
        {
            throw new NotImplementedException();
        }

        public object GetNavigationParameter()
        {
            throw new NotImplementedException();
        }

        public object GetReturningValue()
        {
            throw new NotImplementedException();
        }

        public void GoBack()
        {
            throw new NotImplementedException();
        }

        public void GoBack(object returningValue)
        {
            throw new NotImplementedException();
        }

        public void GoHome()
        {
            throw new NotImplementedException();
        }

        public void Navigate(Type pageViewModelType)
        {
            throw new NotImplementedException();
        }

        public void Navigate(Uri uriToNavigate)
        {
            throw new NotImplementedException();
        }

        public void Navigate(Uri uriToNavigate, object parameters, Dictionary<string, string> queryStrings)
        {
            throw new NotImplementedException();
        }

        public void Navigate(Uri uriToNavigate, object parameters, bool removeFromNavigation = false)
        {
            throw new NotImplementedException();
        }

        public void Navigate<VM>() where VM : IPageViewModel
        {
            throw new NotImplementedException();
        }

        public void Navigate<VM>(object parameters) where VM : IPageViewModel
        {
            throw new NotImplementedException();
        }

        public void Navigate<VM>(object parameters, bool removeFromNavigation = false) where VM : IPageViewModel
        {
            throw new NotImplementedException();
        }

        public void Navigate<VM>(object parameters, Dictionary<string, string> queryStrings) where VM : IPageViewModel
        {
            throw new NotImplementedException();
        }

        public void RemoveBackEntry()
        {
            throw new NotImplementedException();
        }

        public void RemoveLastParameter()
        {
            throw new NotImplementedException();
        }

        public bool TryRestoreFromTombstone()
        {
            throw new NotImplementedException();
        }
    }
}
