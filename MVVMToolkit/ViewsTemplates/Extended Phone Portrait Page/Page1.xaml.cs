﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MVVMToolkit.Views;

namespace $rootnamespace$
{
    public partial class $safeitemname$ : ExtendedPhoneApplicationPage
    {
        public $safeitemname$()
        {
            InitializeComponent();
        }
    }
}