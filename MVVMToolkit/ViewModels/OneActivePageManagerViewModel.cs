﻿using MVVMToolkit.Navigation;
using MVVMToolkit.Navigation.Frame.Contents;
using MVVMToolkit.Navigation.Frame.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace MVVMToolkit.ViewModels
{
    public abstract class OneActivePageManagerViewModel : PageManagerViewModel
    {
        public OneActivePageManagerViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }

        private SemaphoreSlim synchronization = new SemaphoreSlim(1);
        protected FrameManager previouslyManager;


        #region Public methods

        public virtual async void ChangeActiveFrameManager(FrameManager frameManager)
        {
            try
            {
                await synchronization.WaitAsync();

                SelectedManager = frameManager;

                if (frameManager != null)
                {
                    if (previouslyManager != null && previouslyManager.CurrentPageViewModel != null)
                    {
                        previouslyManager.CurrentPageViewModel.IsActive = false;
                        previouslyManager.CurrentPageViewModel.Deactivated();
                    }

                    previouslyManager = frameManager;

                    if (!frameManager.IsFirstPageLoaded)
                    {
                        frameManager.NavigateToStartingPageViewModel(null);
                    }
                    else
                    {
                        if (previouslyManager.CurrentPageViewModel != null && !previouslyManager.CurrentPageViewModel.IsLoaded)
                        {
                            previouslyManager.CurrentPageViewModel.IsLoaded = true;
                            previouslyManager.CurrentPageViewModel.Loaded(null);
                        }

                        if (previouslyManager.CurrentPageViewModel != null && previouslyManager.IsInitialized)
                        {
                            frameManager.CurrentPageViewModel.IsActive = true;
                            frameManager.CurrentPageViewModel.Activated(false);
                        }
                    }
                }
            }
            finally
            {
                synchronization.Release();
            }
        }

        #endregion
    }
}
