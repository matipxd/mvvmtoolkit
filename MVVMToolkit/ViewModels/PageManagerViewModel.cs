﻿using MVVMToolkit.Navigation;
using MVVMToolkit.Navigation.Frame.Contents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace MVVMToolkit.ViewModels
{
    public abstract class PageManagerViewModel : PageViewModelBase
    {
        public event Action<FrameManager> SelectedFrameManagerChanged;

        protected FrameManager _SelectedManager;
        public FrameManager SelectedManager
        {
            get { return _SelectedManager; }
            protected set
            {
                if (_SelectedManager != value)
                {
                    _SelectedManager = value;
                    RaisePropertyChanged("SelectedFrame");

                    if (SelectedFrameManagerChanged != null)
                        SelectedFrameManagerChanged(value);
                }
            }
        }

        public PageManagerViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }
        protected virtual FrameManager GetDefaultStartingManager()
        {
            return null;
        }

        public override bool CanGoBack()
        {
            if (SelectedManager == null)
                SelectedManager = GetDefaultStartingManager();

            if (SelectedManager == null || (SelectedManager != null && SelectedManager.CurrentPageViewModel == null))
                return true;

            return SelectedManager.CurrentPageViewModel.TryGoBack();
        }

        public async override Task<bool> CanGoBackAsync()
        {
            if (SelectedManager == null)
                SelectedManager = GetDefaultStartingManager();

            if (SelectedManager == null || (SelectedManager != null && SelectedManager.CurrentPageViewModel == null))
                return await Task.FromResult(true);

            return await SelectedManager.CurrentPageViewModel.TryGoBackAsync();
        }

        public async override Task OnNavigatedTo(NavigationEventArgs e, object parameter)
        {
            if (SelectedManager == null)
                SelectedManager = GetDefaultStartingManager();

            if (!SelectedManager.IsFirstPageLoaded)
                SelectedManager.NavigateToStartingPageViewModel(parameter);
            else
            {
                if (SelectedManager != null && SelectedManager.CurrentPageViewModel != null)
                {
                    await SelectedManager.CurrentPageViewModel.OnNavigatedTo(e, parameter);

                    if (SelectedManager.IsInitialized)
                    {
                        SelectedManager.CurrentPageViewModel.IsActive = true;
                        await SelectedManager.CurrentPageViewModel.Activated(false);
                    }
                }
            }
        }


        public override async Task OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (SelectedManager == null)
                SelectedManager = GetDefaultStartingManager();

            if (SelectedManager != null && SelectedManager.CurrentPageViewModel != null)
            {
                await SelectedManager.CurrentPageViewModel.OnNavigatedFrom(e);

                SelectedManager.CurrentPageViewModel.IsActive = false;
                await SelectedManager.CurrentPageViewModel.Deactivated();
            }
        }
    }
}
