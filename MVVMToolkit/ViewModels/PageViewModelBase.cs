﻿using Microsoft.Phone.Shell;
using MVVMToolkit.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using MVVMToolkit.Navigation.Frame.Contents;
using System.Diagnostics;

namespace MVVMToolkit.ViewModels
{
    public abstract class PageViewModelBase : ViewModelBase, IPageViewModel
    {
        private System.Windows.FrameworkElement _View = null;
        private IApplicationBar _ApplicationBar = null;
        private System.Windows.Navigation.NavigationContext _NavigationContext = null;


        public INavigationService navigationService { get; private set; }

        public PageViewModelBase(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        public PageViewModelBase()
        {
        }

        public System.Windows.FrameworkElement RootView
        {
            get;
            set;
        }

        ~PageViewModelBase()
        {
            Debug.WriteLine("Released {0}", this);
        }

        public void Navigate<T>(object parameters) where T : IPageViewModel
        {
            if (RootFrame != null)
                RootFrame.Navigate<T>(parameters, false);
            else
            {
                navigationService.Navigate<T>(parameters);
            }
        }

        public void Navigate<T>(object parameters, bool resetNavigationStack, bool keepInstanceLive) where T : IPageViewModel
        {
            if (RootFrame != null)
                RootFrame.Navigate<T>(parameters, resetNavigationStack, keepInstanceLive);
            else
            {
                navigationService.Navigate<T>(parameters);
            }
        }

        public async void OnNavigatedToNavigation(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New || navigationService.IsNavigationFormTombstone)
            {
                var parameters = navigationService.GetNavigationParameter();

                await OnNavigatedTo(e, parameters);

                if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New && !this.IsLoaded)
                {
                    this.IsLoaded = true;
                    await Loaded(parameters);
                }
            }
            else
                if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back)
                {
                    await OnNavigatedTo(e, navigationService.GetReturningValue());
                }
        }

        public async void OnNavigatedFromNavigation(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back && e.IsNavigationInitiator)
            {
                navigationService.RemoveLastParameter();
            }

            await OnNavigatedFrom(e);
        }

        public virtual async Task OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e, object parameter)
        {
            await Task.FromResult(0);
        }

        public virtual async Task OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            await Task.FromResult(0);
        }

        public virtual void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }

        public System.Windows.FrameworkElement View
        {
            get
            {
                return _View;
            }
            set
            {
                _View = value;
            }
        }

        public System.Windows.Navigation.NavigationContext NavigationContext
        {
            get { return _NavigationContext; }
            set { _NavigationContext = value; }
        }

        public Microsoft.Phone.Shell.IApplicationBar ApplicationBar
        {
            get { return _ApplicationBar; }
            set { _ApplicationBar = value; }
        }

        public virtual bool CanGoBack()
        {
            if (RootFrame != null)
                return !RootFrame.CanGoBack();
            else
            {
                return true;
            }
        }

        public virtual bool TryGoBack()
        {
            if (RootFrame != null)
            {
                var canGoBack = RootFrame.CanGoBack();

                if (canGoBack)
                    RootFrame.GoBack();

                return !canGoBack;
            }
            else
            {
                return true;
            }
        }

        public virtual void GoBack()
        {
            if (RootFrame != null)
            {
                RootFrame.GoBack();
            }
            else
            {
                navigationService.GoBack();
            }
        }

        public async virtual Task<bool> TryGoBackAsync()
        {
            if (RootFrame != null)
            {
                var canGoBack = RootFrame.CanGoBack();

                if (canGoBack)
                    RootFrame.GoBack();

                return !canGoBack;
            }
            else
            {
                return await Task.FromResult(true);
            }
        }

        public async virtual Task<bool> CanGoBackAsync()
        {
            if (RootFrame != null)
                return RootFrame.CanGoBack();
            else
            {
                return await Task.FromResult(true);
            }
        }

        public void OnNavigatingFromNavigation(NavigatingCancelEventArgs e)
        {
            OnNavigatingFrom(e);
        }


        public async virtual Task Activated(bool isFirstActivation)
        {
            await Task.FromResult(true);
        }

        public async virtual Task Deactivated()
        {
            await Task.FromResult(true);
        }

        private bool isLoaded;

        public bool IsLoaded
        {
            get { return isLoaded; }
            set
            {
                isLoaded = value;
            }
        }

        public FrameManager RootFrame
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public async virtual Task Loaded(object parameter)
        {
            await Task.FromResult(true);
        }


        public async virtual Task Unloaded()
        {
            await Task.FromResult(true);
        }
    }
}
