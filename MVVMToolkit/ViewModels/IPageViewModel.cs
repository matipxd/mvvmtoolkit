﻿using MVVMToolkit.Navigation.Frame.Contents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkit.ViewModels
{
    public interface IPageViewModel
    {
        FrameManager RootFrame
        {
            get;
            set;
        }

        void OnNavigatedToNavigation(System.Windows.Navigation.NavigationEventArgs e);

        void OnNavigatedFromNavigation(System.Windows.Navigation.NavigationEventArgs e);

        void OnNavigatingFromNavigation(System.Windows.Navigation.NavigatingCancelEventArgs e);

        Task OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e, object parameter);

        Task OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e);

        System.Windows.FrameworkElement View
        {
            get;
            set;
        }

        System.Windows.FrameworkElement RootView
        {
            get;
            set;
        }

        System.Windows.Navigation.NavigationContext NavigationContext
        {
            get;
            set;
        }

        Microsoft.Phone.Shell.IApplicationBar ApplicationBar
        {
            get;
            set;
        }

        bool IsActive
        {
            get;
            set;
        }

        bool CanGoBack();

        Task<bool> CanGoBackAsync();

        Task Activated(bool isFirstActivation);

        Task Deactivated();

        Task Loaded(object parameter);

        Task Unloaded();

        bool TryGoBack();

        Task<bool> TryGoBackAsync();

        bool IsLoaded
        {
            get;
            set;
        }
    }
}
