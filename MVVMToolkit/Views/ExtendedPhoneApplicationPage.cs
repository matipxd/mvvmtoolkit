﻿using Microsoft.Phone.Controls;
using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace MVVMToolkit.Views
{
    public class ExtendedPhoneApplicationPage : PhoneApplicationPage, IExtendedPage
    {
        protected IPageViewModel CurrentViewModel = null;

        public ExtendedPhoneApplicationPage()
        {

            Binding b = new Binding("DataContext") { Source = this };
            var prop = System.Windows.DependencyProperty.RegisterAttached(
                        "ListenDataContext",
                        typeof(object),
                        typeof(ExtendedPhoneApplicationPage),
                        new System.Windows.PropertyMetadata(ListenDataContextPropertyChanged));
                    this.SetBinding(prop, b);
        }

        public bool isFromTombstone = false;

        ~ExtendedPhoneApplicationPage()
        {
            Debug.WriteLine("Released page {0}", this);
        }

        public PhoneApplicationPage RootPhoneApplicationPage
        {
            get
            {
                return this;
            }
        }

        private static void ListenDataContextPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ExtendedPhoneApplicationPage currentview = sender as ExtendedPhoneApplicationPage;

            var ViewModel = currentview.DataContext as IPageViewModel;

            if (ViewModel != null)
            {
                currentview.CurrentViewModel = ViewModel;
                ViewModel.View = currentview;
            }
        }

        protected async override void OnBackKeyPress(CancelEventArgs e)
        {
            if (CurrentViewModel != null)
                e.Cancel = !CurrentViewModel.CanGoBack();

            if (CurrentViewModel != null && e.Cancel == false)
                e.Cancel = ! await CurrentViewModel.CanGoBackAsync();

            base.OnBackKeyPress(e);
        }

        protected override async void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (CurrentViewModel == null)
            {
                if (this.DataContext != null)
                {
                    var ViewModel = this.DataContext as IPageViewModel;

                    if (ViewModel != null)
                    {
                        this.CurrentViewModel = ViewModel;
                        ViewModel.View = this;
                    }
                }
            }

            if (CurrentViewModel != null)
            {
                if (CurrentViewModel.ApplicationBar == null && this.ApplicationBar != null)
                    CurrentViewModel.ApplicationBar = this.ApplicationBar;

                if (CurrentViewModel.NavigationContext == null && this.NavigationContext != null)
                    CurrentViewModel.NavigationContext = this.NavigationContext;

                CurrentViewModel.OnNavigatedToNavigation(e);

                CurrentViewModel.IsActive = true;
                await CurrentViewModel.Activated(true);
            }
        }

        protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (CurrentViewModel != null)
            {
                CurrentViewModel.OnNavigatingFromNavigation(e);
            }

            base.OnNavigatingFrom(e);
        }

        protected override async void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (CurrentViewModel != null)
            {
                CurrentViewModel.OnNavigatedFromNavigation(e);

                CurrentViewModel.IsActive = false;
                await CurrentViewModel.Deactivated();

            }

            base.OnNavigatedFrom(e);
        }
    }
}
