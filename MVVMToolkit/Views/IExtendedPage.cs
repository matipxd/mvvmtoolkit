﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkit.Views
{
    public interface IExtendedPage
    {
        PhoneApplicationPage RootPhoneApplicationPage
        {
            get;
        }
    }
}
