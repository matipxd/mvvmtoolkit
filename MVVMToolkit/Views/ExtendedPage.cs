﻿using Microsoft.Phone.Controls;
using MVVMToolkit.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MVVMToolkit.Views
{
    public class ExtendedPage : UserControl, IExtendedPage
    {
        internal PhoneApplicationPage rootPhoneApplicationPage;

        public PhoneApplicationPage RootPhoneApplicationPage
        {
            get
            {
                if (PageManager.Instance.TopFrame == null)
                    return null;
                else
                    return PageManager.Instance.TopFrame.Content as PhoneApplicationPage;
            }
        }
    }
}
