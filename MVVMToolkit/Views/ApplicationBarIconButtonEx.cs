﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkit.Views
{
    public class ApplicationBarIconButtonEx : ApplicationBarIconButton
    {
        private int lockCount = 0;
        private Object internalLock = new Object();
        private bool isEnabled = false;
        private bool isDisabled = false;
        public void Disable()
        {
            lock (internalLock)
            {
                isDisabled = true;
                isEnabled = base.IsEnabled;
                base.IsEnabled = false;
            }
        }

        public void Enable()
        {
            lock (internalLock)
            {
                base.IsEnabled = isEnabled;
                isDisabled = false;
            }
        }

        public void Lock()
        {
            lock (internalLock)
            {
                lockCount++;

                if (lockCount > 0)
                    DisableUIObject();
            }
        }

        public virtual void DisableUIObject()
        {
            lock(internalLock)
            {
                if (isDisabled)
                    isEnabled = false;
                else
                    base.IsEnabled = false;
            }
        }

        public virtual void EnableUIObject()
        {
            lock (internalLock)
            {
                if (isDisabled)
                    isEnabled = true;
                else
                    base.IsEnabled = true;
            }
        }

        public void Unlock()
        {
            lock (internalLock)
            {
                if (lockCount > 0)
                    lockCount--;

                if (lockCount <= 0)
                    EnableUIObject();
            }
        }

        public void ResetLock()
        {
            lock (internalLock)
            {
                lockCount = 0;

                EnableUIObject();
            }
        }

        public new bool IsEnabled
        {
            get
            {
                return base.IsEnabled;
            }
            set
            {
                if (value)
                    EnableUIObject();
                else
                    DisableUIObject();
            }
        }

    }
}
