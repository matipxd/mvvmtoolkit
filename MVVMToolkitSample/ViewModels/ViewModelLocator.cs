﻿using MVVMToolkit.IoC;
using MVVMToolkit.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkitSample.ViewModels
{
    public class ViewModelLocator
    {
        private static bool initialized = false;

        public ViewModelLocator()
        {
            if (initialized)
                return;

            initialized = true;

            SimpleIoc.Default.Register<INavigationService>(() =>
            {
                return new NavigationService(App.RootFrame, App.RootFrame.Dispatcher);
            });

            SimpleIoc.Default.Register<Page1ViewModel>();
            SimpleIoc.Default.Register<Page2ViewModel>();
            SimpleIoc.Default.Register<MainPageViewModel>();
        }

        public MainPageViewModel MainPage
        {
            get
            {
                return SimpleIoc.Default.GetInstance<MainPageViewModel>();
            }
        }
    }
}
