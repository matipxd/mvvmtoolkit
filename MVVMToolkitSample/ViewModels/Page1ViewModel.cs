﻿using MVVMToolkit.Navigation;
using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkitSample.ViewModels
{
    public class Page1ViewModel : PageViewModelBase
    {
        public Page1ViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }
    }
}
