﻿using MVVMToolkit.IoC;
using MVVMToolkit.Navigation;
using MVVMToolkit.Navigation.Frame.Contents;
using MVVMToolkit.ViewModels;
using MVVMToolkitSample.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkitSample.ViewModels
{
    public class MainPageViewModel : OneActivePageManagerViewModel
    {
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }

        private FramePageContent<Page1, Page1ViewModel> page1;
        private FramePageContent<Page2, Page2ViewModel> page2;


        public FramePageContent<Page1, Page1ViewModel> Page1
        {
            get
            {
                if (page1 == null)
                    page1 = new FramePageContent<Page1, Page1ViewModel>(() => new Page1(), () => SimpleIoc.Default.GetInstance<Page1ViewModel>());

                return page1;
            }
        }

        public FramePageContent<Page2, Page2ViewModel> Page2
        {
            get
            {
                if (page2 == null)
                    page2 = new FramePageContent<Page2, Page2ViewModel>(() => new Page2(), () => SimpleIoc.Default.GetInstance<Page2ViewModel>());

                return page2;
            }
        }
    }
}
