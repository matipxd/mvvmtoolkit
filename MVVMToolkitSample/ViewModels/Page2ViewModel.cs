﻿using MVVMToolkit.Navigation;
using MVVMToolkit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMToolkitSample.ViewModels
{
    public class Page2ViewModel : PageViewModelBase
    {
        public Page2ViewModel(INavigationService navigationService)
            : base(navigationService)
        {

        }
    }
}
